a instala��o � bem simples:

1)-Com o Delphi aberto feche todos os arquivos que porventura estejam abertos (op��o File | Close All).

2)-A seguir abra o arquivo chamado "RXCTL7.DPK" (para o Delphi 7, caso esteja usando outra vers�o basta substituir o "7" pelo n�mero da sua vers�o).
   Clique no bot�o "Compile" e depois feche a janela.

3)-Repita o mesmo procedimento para os arquivos "RXDB7.DPK" e "RXBDE7.DPK".
   Foram gerados alguns arquivos BPL (RXCTL7.BPL, RXDB7.BPL e RXBDE7.BPL). Copie-os para a pasta \\WINDOWS\\SYSTEM32.

4)-Depois disto abra o arquivo DCLRX7.DPK. Clique no bot�o "Compile" e em seguida no bot�o "Install".
   Vai instalar v�rios componentes. Feche a janela e clique no bot�o para n�o salvar o arquivo quando o Delphi perguntar.

5)-Repita o mesmo procedimento para os arquivos "DCLRXDB7.DPK" e "DCLRXBD7.DPK". Lembre-se sempre de n�o salvar o arquivo quando fechar a janela de instala��o dos componentes.



--> Se tudo correu bem, neste ponto todos os componentes que comp�em a RX-Lib j� foram instalados !

